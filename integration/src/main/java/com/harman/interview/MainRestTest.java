package com.harman.interview;

import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import static com.jayway.restassured.RestAssured.given;

@RunWith(JUnitParamsRunner.class)
public class MainRestTest {

  @Test
  public void shouldReturn200WhenGetGoogle() {
    // @formatter:off
    given()
    .when()
        .get("http://google.com")
    .then().log().all()
        .statusCode(HttpStatus.OK.value());
    // @formatter:on
  }
}
